package main

import (
	"github.com/gin-gonic/gin"

	"gorm-gin/models"

	"gorm-gin/controllers"
)

func main() {
	r := gin.Default()

	// r.GET("/ping", func(c *gin.Context) {
	// 	c.JSON(http.StatusOK, gin.H{
	// 		"message": "pong",
	// 	})
	// })

	models.ConnectDatabase()

	r.GET("/books", controllers.AllBooks)
	r.POST("/books", controllers.CreateBook)
	r.GET("/books/:id", controllers.FindBook)
	r.PATCH("/books/:id", controllers.UpdateBook)
	r.DELETE("/books/:id", controllers.DeleteBook)
	r.Run()

}
